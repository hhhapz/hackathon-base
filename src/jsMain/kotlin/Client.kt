import org.w3c.xhr.XMLHttpRequest
import kotlin.browser.document


const val url = "http://localhost:8080/random"

fun main() {
    val buttonNum = document.getElementById("buttonNum")
    val num = document.getElementById("num")
    val buttonName = document.getElementById("buttonName")
    val name = document.getElementById("name")

    buttonNum!!.addEventListener("click", {
        val xhttp = XMLHttpRequest()
        xhttp.open("GET", "$url/num")
        xhttp.send()

        xhttp.onreadystatechange = {
            num?.innerHTML = xhttp.responseText
            Unit
        }
    })

    buttonName!!.addEventListener("click", {
        val xhttp = XMLHttpRequest()
        xhttp.open("GET", "$url/name")
        xhttp.send()

        xhttp.onreadystatechange = {
            name!!.innerHTML = xhttp.responseText
            Unit
        }
    })


}