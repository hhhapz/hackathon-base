package id.or.jisedu.hackaton

import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.CORS
import io.ktor.features.ContentNegotiation
import io.ktor.jackson.jackson
import io.ktor.response.respond
import io.ktor.routing.get
import io.ktor.routing.route
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import kotlin.random.Random

fun main() {
    startServer()
}

fun startServer() {
    embeddedServer(Netty, 8080) {

        install(ContentNegotiation) {
            jackson { }
        }

        install(CORS) {
            anyHost()
        }

        routing {
            route("/random") {
                get("num") { call.respond(Random.nextInt(1000)) }
                get("name") { call.respond(listOf("John", "Bob", "Steven", "Joe", "Nazule", "Jimmy").shuffled().first()) }

            }
        }
    }.start(wait = true)
}