# Kotlin Hackathon base

## What is this for?
Tomorrow (October 27th) I will be attending a charity Hackathon to help people in need and make some cool stuff

## What's this?

This is a simple bare bones example of a multiplatform application (that ironically uses no multiplatform capabilities **yet –– I will add some later**).
It is all written in Kotlin, and is a Simple API that i called on random/number to return a random number, and random/name to return a random name.
The numbers are called via the rest api in Kotlin and that is transpiled to JavaScript.

## How to play with this?

Prerequisites:
Kotlin
Gradle
Java

I haven't tried this in any other IDE other than Intellij IDEA. You will need the 1.3.0-rc-190. Start off by running the server. If you make updates
to the client module, you're gonna have to build it by running `gradle build`. This will compile the JavaScript in the build folder, and it is 
already added to a base HTML file in the root directory.

## Running and working example [here](https://cdn.discordapp.com/attachments/244230771232079873/505383976207253508/RestAPI_Kotlin_MPP.mov)